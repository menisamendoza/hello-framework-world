<?php

namespace app\validator;

use rueckgrat\security\ValidationRules;
use rueckgrat\security\ValidationRule;
/**
 * Description of UserValidator
 *
 * @author menisamendoza
 */
class UserValidator extends \rueckgrat\security\ValidatorContainer{
    public function __construct(\app\mapper\User $user) {
        parent::__construct($user);
        
        $prename = new ValidationRule('prename',  ValidationRules::MIXED);
        $prename->setLengths(3, 50);
        $prename->setErrorMsgGlobal("Please enter a prename");
        
        $this->addRule($prename);
        
        $lastname = new ValidationRule('lastname',  ValidationRules::MIXED);
        $lastname->setLengths(3, 50);
        $lastname->setErrorMsgGlobal("Please enter a lastname");
        
        $this->addRule($lastname);
        
        $email = new ValidationRule('email',  ValidationRules::EMAIL);
        $email->setLengths(3, 50);
        $email->setErrorMsgGlobal("Please enter an email");
        
        $this->addRule($email);
    }
}
