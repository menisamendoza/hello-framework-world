<?php

namespace app\model;

/**
 * Description of UserModel
 *
 * @author menisamendoza
 */
class UserModel extends \rueckgrat\mvc\DefaultDBModel{
    
    protected $validator;


    public function __construct() {
        parent::__construct("user");
        
        $this->validator= new \rueckgrat\security\InputValidator();
    }
    
    public function getAllUsers(){
        $stmt = $this->db->query("SELECT * FROM user");
        
        $users = array();
        
        while($row = $stmt->fetch()){
            $user =new \app\mapper\User();
            $user->map($row);
            
            //print_r($row);
            $users[]=$user;
        }
        
        return $users;
    }
    
    public function createUser(\app\mapper\User $user){
        $this->validator->validate(new \app\validator\UserValidator($user));
        $this->create($user);
    }
    
    public function getById($id){
        $user = new \app\mapper\User();
        
        $row = $this->get($id);
        $user->map($row);
        return $user;
    }
    
    public function deleteUser(\app\mapper\User $user){
        $this->delete($user);
    }
    
    public function editUser(\app\mapper\User $user){
        $this->validator->validate(new \app\validator\UserValidator($user)); 
        $this->save($user);
    }
}
