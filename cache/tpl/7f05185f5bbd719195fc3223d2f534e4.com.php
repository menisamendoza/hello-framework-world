<!DOCTYPE html>
<html>
    <head>
        <title><?= $this->title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="public/css/style.css">
            </head>
    <body>
        <div id="wrapper">
            <h1>User</h1>

<table>
    <tr>
        <th>ID</th>
        <th>Prename</th>
        <th>Lastname</th>
        <th>E-mail</th>
    </tr>
    <?php
    foreach ($this->users as $user){
        ?>
    <tr>
        <td><?php echo $user->getId();?></td>
        <td><?php echo $user->getPrename();?></td>
        <td><?php echo $user->getLastname();?></td>
        <td><?php echo $user->getEmail();?></td>
        <td><a href="?c=Main&m=delete&id=<?php echo $user->getId();?>">Delete</a></td>
        <td><a href="?c=Main&m=edit&id=<?php echo $user->getId();?>">Edit</a></td>
    </tr>
    <?php
    }
    ?>
</table>
<form action="index.php" method="POST">
    <input type="hidden" name="c" value="Main"/>
    <input type="hidden" name="m" value="create"/>
    Prename:
    <input name="prename" type="text" value=""/>
    Lastname:
    <input name="lastname" type="text" value=""/>
    E-mail:
    <input name="email" type="text" value=""/>
    <input type="submit" value="SAVE"/>
</form>        </div>
        <?php
        $cb = \rueckgrat\xhr\CallbackManager::getCallbacks();
        if (count($cb) > 0) {
            echo '<script type="text/javascript"">$(document).ready(function(){handleCallbacks(' . json_encode(array('callbacks' => $cb)) . ');});</script>' . "\n";
        }
        ?>
                <script type="text/javascript" src="public/js/main.js"></script>
    </body>
</html>