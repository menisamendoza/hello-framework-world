<?php

namespace app\dbo;

/**
 * Description of User
 *
 * @author menisamendoza
 */
class User extends \rueckgrat\db\Mapper {
    
    protected $prename;
    protected $lastname;
    protected $email;

    function getPrename() {
        return $this->prename;
    }

    function getLastname() {
        return $this->lastname;
    }

    function getEmail() {
        return $this->email;
    }

    public function __construct() {
        parent::__construct();
    }
}
